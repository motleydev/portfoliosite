var $ = require('jquery');
var React = require('react/addons');
var Hammer = require('react-hammerjs');
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
$('#filter-grid--quotes').each(function(){

			/**
			*
			* State Master
			*
			**/
			var StateMaster = {
				state: {
					quoteFilters: authors,
					activeQuote: null
				},
				resetAllFilters: function(){
					for (var filter in this.state.horseFilters)
					{
						var currFilter = this.state.horseFilters[filter]
						if(currFilter.inputType == 'text') {
							currFilter.value = '';
						}
						else
						{
							currFilter.value = false;
						}
					}
				},
				togglequoteFilters: function(filter) {
					this.state.quoteFilters[filter].visibility = !this.state.quoteFilters[filter].visibility;
					this.emit('quote-search-term-visibility-change');
				},
				isolatequoteFilters: function(filter) {
					this.resetAllFilters;
					this.state.quoteFilters[filter].visibility = true;
					this.emit('quote-search-term-visibility-change');
				},
				setActiveQuote: function(quote){
					this.state.activeQuote = quote;
					this.emit('new-active-quote');
				},

  				// this is just a super-minimal EventEmitter implementation
  				_eventHandlers: {},
  				on: function(evtName, evtHandler) {
  					this._eventHandlers[evtName] || (this._eventHandlers[evtName] = []);
  					this._eventHandlers[evtName].push(evtHandler);
  				},
  				emit: function(evtName, data) {
  					var handlers = this._eventHandlers[evtName];
  					if (!handlers) return;
  					handlers.forEach(function(handler) { handler.call(null, data); });
  				}
  			};


			/**
			*
			* The Controller Form
			*
			**/
			
			var ControllerForm = React.createClass({

				render: function(){
					var filters = this.props.listFilters;
					var textboxes = [];
					var checkboxes = [];
					

					for (var filter in filters.quoteFilters){
						if(filters.quoteFilters[filter].inputType == 'text'){

							textboxes.push(
								<section key={filter}>
									<input
									className="search"
									placeholder={filters.quoteFilters[filter].label}
									type="text"
									name={filter}
									value={filters.quoteFilters[filter].visibility}
									onChange={StateMaster.togglequoteFilters.bind(
										StateMaster,
										filter)}/>
								</section>
								);

							continue;
						} else {

							checkboxes.push(
								<section key={filter}>
									<input
									type="checkbox"
									id={filter}
									checked={filters.quoteFilters[filter].visibility}
									onChange={StateMaster.togglequoteFilters.bind(StateMaster, filter)}/>
									<label htmlFor={filter}>{filters.quoteFilters[filter].name}</label>
								</section>
								)	
						}
						
					};
					return (
						<form>
							{checkboxes}
						</form>
						)
				}
			});


			/**
			*
			* The Controller
			*
			**/

			var GridController = React.createClass({
				render: function() {
					return (
						<section className="controller">
							<section className="controller-wrapper">
								<ControllerForm {...this.props} />
							</section>
						</section>
						)
				}
			});			

			/**
			*
			* The List Item
			*
			**/
			

			var GridItem = React.createClass({
				getInitialState: function(){
					return {activeQuote: StateMaster.state.activeQuote};
				},
				handleClick: function() {
					if (this.props.item.id === this.state.activeQuote){
						StateMaster.setActiveQuote(null);	
					} else
					{
						StateMaster.setActiveQuote(this.props.item.id);	
					}
				},
				componentDidMount: function(){
					var self = this;
					StateMaster.on('new-active-quote', function(){
						self.setState({activeQuote: StateMaster.state.activeQuote})
					});
				},
				render: function() {
					var cx = React.addons.classSet;
					var classes = cx({
						'active': this.props.item.id === this.state.activeQuote,
						'quote--item': true
					});
					var style={
						backgroundImage: 'url(/images/'+this.props.item.source.id+'.jpg)'
					};
					var quoteImage = (<section style={style} className="quote--image"> </section>);
					var quoteContent;
					//if (this.props.item.id === this.state.activeQuote){
						if (1===1){
							quoteContent = (
								<section>
									<section className="quote--stats">
										<span className="quote-mark">&ldquo;</span>
										<blockquote>
											<span className="quote" >{this.props.item.quote}</span>
											<cite><span className="author">{this.props.item.source.name}</span></cite>
										</blockquote>
										<span className="quote-mark">&rdquo;</span>
									</section>
									<section className="buffer"> </section>
								</section>);
						} else {
							quoteContent = (<section className="buffer"></section>);
						}

						return(
							<section className={classes} onClick={this.handleClick}>
								{quoteImage}
								{quoteContent}
							</section>
							)}
					});

			/**
			*
			* The List
			*
			**/
			

			var GridList = React.createClass({

				render: function() {
					var theResult = [];
					var filters = this.props.listFilters.quoteFilters;
					var self = this;
					this.props.items.forEach(function(quote){
						var quoteListing = (<GridItem key={quote.id} item={quote} />);
						
						for (key in filters){
							var currFilter = filters[key];
							var currFilterId = currFilter.id
							
							if ((quote.source.id === currFilterId) && filters[currFilter.id].visibility) {
								theResult.push(quoteListing);
							}
						}

						
					});

					return (<section className="quote--list">
						<ReactCSSTransitionGroup transitionName="quote">
						{theResult}
						</ReactCSSTransitionGroup>
					</section>)
				}
			});

			/**
			*
			* The Actual Component
			*
			**/
			

			var FilterGrid = React.createClass({
				getInitialState: function() {

					return { quoteFilters: StateMaster.state.quoteFilters };
				},
				componentDidMount: function() {
					var self = this;

					StateMaster.on('quote-search-term-visibility-change', function(newquoteFilters) {
						self.setState({ quoteFilters: StateMaster.state.quoteFilters });
					});
				},

				render: function() {
					return (<section className="wrapper filter-grid">
						<GridController listFilters={this.state} />
						<GridList listFilters={this.state} items={quotes} />
					</section>)
				}
			});

			React.render(
				<FilterGrid/>,
				document.getElementById('filter-grid--quotes')
				);


});