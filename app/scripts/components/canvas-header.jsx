// /** @jsx React.DOM */

// var React = require('react');
// // var ReactCanvas = require('react-canvas');

// var Surface = ReactCanvas.Surface;
// var Group = ReactCanvas.Group;
// var Image = ReactCanvas.Image;
// var Text = ReactCanvas.Text;
// var FontFace = ReactCanvas.FontFace;

// var App = React.createClass({

//   componentDidMount: function () {
//     window.addEventListener('resize', this.handleResize, true);
//   },

//   render: function () {
//     var size = this.getSize();
//     return (
//       <Surface top={0} left={0} width={size.width} height={size.height} enableCSSLayout={true}>
//       <Group style={this.getPageStyle()}>
//       <Text style={this.getTitleStyle()}>
//       Professor PuddinPop
//       </Text>
//       <Text style={this.getExcerptStyle()}>
//       We are the few, the happy few, the band of brothers.
//       </Text>
//       </Group>
//       </Surface>
//       );
// },

//   // Styles
//   // ======

//   getSize: function () {
//     return document.getElementById('manifestoLoader').getBoundingClientRect();
//   },

//   getPageStyle: function () {
//     var size = this.getSize();
//     return {
//       position: 'relative',
//       padding: 14,
//       width: size.width,
//       height: size.height,
//       flexDirection: 'column'
//     };
//   },

//   getTitleStyle: function () {
//     return {
//       fontFace: FontFace('Georgia'),
//       fontSize: 18,
//       lineHeight: 28,
//       height: 28,
//       marginBottom: 10,
//       color: '#333',
//       textAlign: 'center'
//     };
//   },

//   getExcerptStyle: function () {
//     return {
//       fontFace: FontFace('Georgia'),
//       fontSize: 100,
//       lineHeight: 150,
//       marginTop: 15,
//       flex: 1,
//       color: '#333'
//     };
//   },

//   // Events
//   // ======

//   handleResize: function () {
//     this.forceUpdate();
//   }

// });

// function render(){
//   return <App />
// }


// module.exports = render;