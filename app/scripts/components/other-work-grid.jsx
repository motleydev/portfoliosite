var $ = require('jquery');
var React = require('react/addons');
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

			/**
			*
			* State Master
			*
			**/
			var StateMaster = {
				state: {
					skillFilters: skillsCategories, 
					activeSkill: null
				},
				toggleSkillFilters: function(filter) {
					this.state.skillFilters[filter].value = !this.state.skillFilters[filter].value;
					this.emit('skill-search-term-visibility-change');
				},

				setActiveSkill: function(skill){
					this.state.activeSkill = skill;
					this.emit('new-active-skill');
				},

  				// this is just a super-minimal EventEmitter implementation
  				_eventHandlers: {},
  				on: function(evtName, evtHandler) {
  					this._eventHandlers[evtName] || (this._eventHandlers[evtName] = []);
  					this._eventHandlers[evtName].push(evtHandler);
  				},
  				emit: function(evtName, data) {
  					var handlers = this._eventHandlers[evtName];
  					if (!handlers) return;
  					handlers.forEach(function(handler) { handler.call(null, data); });
  				}
  			};


			/**
			*
			* The Controller Form
			*
			**/
			
			var ControllerForm = React.createClass({

				render: function(){
					var filters = this.props.listFilters;
					var textboxes = [];
					var checkboxes = [];
					

					for (var filter in filters.skillFilters){
						if(filters.skillFilters[filter].inputType == 'text'){

							textboxes.push(
								<section key={filter}>
									<input
									className="search"
									placeholder={filters.skillFilters[filter].label}
									type="text"
									name={filter}
									value={filters.skillFilters[filter].value}
									onChange={StateMaster.toggleSkillFilters.bind(
										StateMaster,
										filter)}/>
								</section>
								);

							continue;
						} else {

							checkboxes.push(
								<section key={filter}>
									<input
									type="checkbox"
									id={filter}
									checked={filters.skillFilters[filter].value}
									onChange={StateMaster.toggleSkillFilters.bind(StateMaster, filter)}/>
									<label htmlFor={filter}>{filters.skillFilters[filter].name[locale]}</label>
								</section>
								)	
						}
						
					};
					return (
						<form>
							{checkboxes}
						</form>
						)
				}
			});


			/**
			*
			* The Controller
			*
			**/

			var GridController = React.createClass({
				render: function() {
					return (
						<section className="controller">
							<section className="controller-wrapper">
								<ControllerForm {...this.props} />
							</section>
						</section>
						)
				}
			});


			/**
			*
			* The List Item
			*
			**/
			

			var GridItem = React.createClass({
				getInitialState: function(){
					return {activeSkill: StateMaster.state.activeSkill};
				},
				handleClick: function(){
					if (this.props.item.id === this.state.activeSkill){
						StateMaster.setActiveSkill(null);	
					} else
					{
						StateMaster.setActiveSkill(this.props.item.id);	
					}
					
				},
				componentDidMount: function(){
					var self = this;
					StateMaster.on('new-active-skill', function(){
						self.setState({activeSkill: StateMaster.state.activeSkill})
					});
				},
				render: function() {
					var cx = React.addons.classSet;
					var classes = cx({
						'active': this.props.item.id === this.state.activeSkill,
						'skill--item': true
					});
					var stars = [];
					
					for (i=0; i < this.props.item.skill_level; i++)
					{
						var uniKey = "star" +i;
						stars.push(<span key={uniKey} className="star"></span>);
					}

					var articleStyle = {
						backgroundImage: 'url(/images/'+this.props.item.image+')'
					}
					return(
						<section onClick={this.handleClick} className={classes}>
							<section style={articleStyle} className="skill--image">
								<article>
									<header>{this.props.item.title[locale]}</header>
									<span dangerouslySetInnerHTML ={{__html: this.props.item.content[locale] }} />
								</article>
							</section>
							<ul>
								<li className="name">{this.props.item.title[locale]}</li>
								<li className="rating">{stars}</li>
								<li className="category">{this.props.item.category.name[locale]}</li>
							</ul>
						</section>
						)}
				});

			/**
			*
			* The List
			*
			**/
			

			var GridList = React.createClass({

				render: function() {
					var theResult = [];
					var filter = this.props.listFilters.skillFilters;
					var self = this;
					this.props.items.forEach(function(skill){
						var skillListing = (<GridItem key={skill.id} item={skill} />);

						if ((skill.category.design && filter.cat_1.value)) {
							theResult.push(skillListing);
						}

						if ((skill.category.productivity && filter.cat_2.value)) {
							theResult.push(skillListing);
						}

						if ((skill.category.tech && filter.cat_3.value)) {
							theResult.push(skillListing);
						}

						return;
						
						
					});

					return (<section className="skill--list">
						<ReactCSSTransitionGroup transitionName="skill">
						{theResult}
						</ReactCSSTransitionGroup>
					</section>)
				}
			});

			/**
			*
			* The Actual Component
			*
			**/
			

			var FilterGrid = React.createClass({
				getInitialState: function() {

					return { skillFilters: StateMaster.state.skillFilters };
				},
				componentDidMount: function() {
					var self = this;

					StateMaster.on('skill-search-term-visibility-change', function(newskillFilters) {
						self.setState({ skillFilters: StateMaster.state.skillFilters });
					});
				},

				render: function() {
					return (<section className="wrapper filter-grid">
						<GridController listFilters={this.state} />
						<GridList listFilters={this.state} items={skillsArr} />
					</section>)
				}
			});
			

module.exports.FilterGrid = FilterGrid;