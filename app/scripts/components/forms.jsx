var React = require('react/addons');
var forms = require('newforms');
var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;
var $ = require('jquery');

var ContactForm = forms.Form.extend({
  Veritas: forms.CharField({
  	widget: forms.HiddenInput({}),
    required: false,
  }),
  Name: forms.CharField({
    label: "First & Last Name",
    required: false
  }),
  Email: forms.EmailField({
    required: false
  }),
  Website: forms.URLField({
    required: false
  }),
  Message: forms.CharField({
  	widget: forms.Textarea({attrs: {rows: 6, cols: 60},
  })})
});


var Contact = React.createClass({

  onContact: function(data){
    var self = this;
    $.ajax({
        url: "/contact",
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        success: function(data) {
            self.props.userSubmission();
        },
        error: function() {
            console.log('error');
        }
      }
    );

  },

  handleContact: function(data){
  	console.log(data);
  },

  _onSubmit: function(e) {
    e.preventDefault()

    var form = this.refs.contactForm.getForm();
    var isValid = form.validate();
    if (isValid) {
      this.onContact(form.cleanedData)
    }
  },
  render: function() {
    return (<form onSubmit={this._onSubmit} method="post" action="/contact">
      <forms.RenderForm form={ContactForm} ref="contactForm"/>
      <input type="submit" value="Submit" />{' '}
    </form>)
  }
});


var MyContactForm = React.createClass({
  getInitialState: function(){
    return {submitted: false}
  },
  setStatus: function(){
    this.setState({submitted: true})
  },
  render: function(){
    if(this.state.submitted == false)
    {
      var content =  (<Contact key="form" userSubmission={this.setStatus} />)
    }
    else if(this.state.submitted == true)
    {
      var content = (<section key="thanks">"Thank you!!"</section>);
    }
    return (<section>
      <ReactCSSTransitionGroup transitionName="form">
      {content}
      </ReactCSSTransitionGroup>
      </section>)
  }

})

module.exports.Contact =  MyContactForm;