var $ = require('jquery');
var React = require('react/addons');



/**
*
* State Controller
*
**/
var StateMaster = {
				state: {
					modalOpen: false
				},
				openModal: function(obj) {
					this.state.modalOpen = true;
					$('body').addClass('lock-down');
					this.emit('modal-is-now-open', obj);
				},

				closeModal: function() {
					this.state.modalOpen = false;
					$('body').removeClass('lock-down');
					this.emit('modal-is-now-closed');
				},

  				// this is just a super-minimal EventEmitter implementation
  				_eventHandlers: {},
  				on: function(evtName, evtHandler) {
  					this._eventHandlers[evtName] || (this._eventHandlers[evtName] = []);
  					this._eventHandlers[evtName].push(evtHandler);
  				},
  				emit: function(evtName, data) {
  					var handlers = this._eventHandlers[evtName];
  					if (!handlers) return;
  					handlers.forEach(function(handler) { handler.call(null, data); });
  				}
  			};



/*=======================================
=            Modal Components            =
=======================================*/



/**
*
* Modal Control
*
**/

var ModalControl = React.createClass({
	
	handleClick: function(event){
		StateMaster.closeModal();
	},
	render: function(){
		return (<span role="button" onClick={this.handleClick}>X</span>)
	}
});


/**
*
* Modal
*
**/

var Modal = React.createClass({
	getInitialState: function(){
		return { modalOpen: StateMaster.state.modalOpen };
	},
	componentDidMount: function(){
		var self = this;
		StateMaster.on('modal-is-now-open', function(data) {
			self.setState({ modalOpen: true });
			self.image = data.image;
			self.imageSize = data.size;
			self.description = data.description[locale]
		});
		StateMaster.on('modal-is-now-closed', function() {
			self.setState({ modalOpen: false })
		});

	window.addEventListener("keyup", this.handleClick);

	},
	handleClick: function(){
		StateMaster.closeModal();
	},
	render: function(){
		var cx = React.addons.classSet;
  		var classes = cx({
    		'modal': true,
    		'modal-is-open': this.state.modalOpen,
    		'modal-is-closed': this.state.modalOpen ? false : true
  		});
  		var style = {
			backgroundImage: 'url(/images/'+this.image+')'
		};
		var image = this.image ? '/images/'+this.image : '/images/mountain.jpg';
		return (<section onClick={this.handleClick} className={classes}>
			<section>
				<ModalControl />
				<img src={image} height="90%" />
				<p>{this.description}</p>
			</section>
			</section>)
	}
});






/*========================================
=            Image Component            =
========================================*/

/**
*
* Individual Images
*
**/

var Image = React.createClass({

handleClick: function(event){
	StateMaster.openModal(this.props.details);
},

render: function () {
	var img = this.props.details.image;
	var articleStyle = {
		backgroundImage: 'url(/images/'+img+')'
	}
	return (<article style={articleStyle} onClick={this.handleClick} ></article>)
}

});

/**
*
* All Images
*
**/

var Images = React.createClass({
  render: function () {
  	var ImagesArr = this.props.images.map(function(image, index){
  		var uniKey = "image_"+index;
  		return (<Image key={uniKey} details={image} />)
  	});

  	return (<section>{ImagesArr}</section>)

}

});


$('.modal-images').each(function() {
	$('body').append('<section id="modal-loader"></section>');
	React.render(<Modal />, document.getElementById('modal-loader'));
	var images = $(this).data('images');
	React.render(<Images images={images} />, this);
});


var MyModal = {};
MyModal.Modal = Modal;
MyModal.Images = Images;

module.exports.MyModal = MyModal;