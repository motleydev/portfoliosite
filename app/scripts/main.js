var $ = require('jquery');
var td = require('throttle-debounce');
var React =	require('react/addons');



$.extend(td);




/**
*
* React Components
*
**/

var modalPlugin = require('./components/gallery-with-modal.jsx');
var MyModal  = modalPlugin.MyModal.Modal;
var Images  = modalPlugin.MyModal.Images;

var Contact = require('./components/forms.jsx');
var ContactForm = require('./components/forms.jsx').Contact;

var FilterGrid  = require('./components/other-work-grid.jsx').FilterGrid;
var quotes 		= require('./components/quotes-grid.jsx');


var transitionEnd = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend';


/**
*
* Helper functions
*
**/


function toggleActive(item){
	item.parent().siblings().removeClass('active');
	item.parent().addClass('active');
}

function smoothScroller(el){
	$('html, body').animate({
		scrollTop: el.offset().top
	}, 500);
}



/**
*
* Handle Ajaxy navigations
*
**/


$('#thisPage').on('click','a',function(e){
	e.preventDefault();
	history.pushState(null, null, $(this).attr('href'));
	toggleActive($(this));
	$('.content').load($(this).attr('href')+' #stage', function(){

		if ($('#filter-grid--other').length) {
			React.render(
				<FilterGrid />,
				document.getElementById('filter-grid--other')
				);

			smoothScroller($('.nav--bottom'));

		};

		// Make the first item active
		var firstListItem = $('#brandMenu ul li:first-child a');
		toggleActive(firstListItem);
		$('.slider--brands').load(firstListItem.attr('href')+'/slide', function(){
			$('#brand-detail-loader').load(firstListItem.attr('href')+' #brand-detail', function(){
				history.pushState(null, null, firstListItem.attr('href'));


					// Create my modal loader zone
					if ($('.modal-images').length){

						$('body').append('<section id="modal-loader"></section>');
						React.render(<MyModal />, document.getElementById('modal-loader'));
					}
					//Render my modal.
					$('.modal-images').each(function() {
						var images = $(this).data('images');
						React.render(<Images images={images} />, this);
					});
					$('.slider--brands').find('article').addClass('loaded');
					smoothScroller($('.nav--bottom'));
				});
		});
	});
});



$('.content').on('click','#brandMenu a',function(e){
	e.preventDefault();
	toggleActive($(this));
	var self = $(this);
	$('.slider--brands').load($(this).attr('href')+'/slide', function(){
		$('.slider--brands').find('article').addClass('loaded');
		$('#brand-detail-loader').load(self.attr('href')+' #brand-detail', function(){

			history.pushState(null, null, self.attr('href'));
				// Create my modal loader zone
				if ($('.modal-images').length){
					React.render(<MyModal />, document.getElementById('modal-loader'));
				}
				//Render my modal.
				$('.modal-images').each(function() {
					var images = $(this).data('images');
					React.render(<Images images={images} />, this);
				});
			});
	});
});




/**
*
* Load React Components for static landing endpoints.
*
**/



$('#contact-form').each(function(){
	React.render(
		<ContactForm />,
		document.getElementById('contact-form'))
});

$('#filter-grid--other').each(function(){
	React.render(
		<FilterGrid />,
		document.getElementById('filter-grid--other')
		);
})



var NotReady = React.createClass({
	handleClick: function(){
		var self = this;
		$.ajax({
			url: "/dismiss",
			type: "POST",
			success: function(data) {
				$('#alertmodal').remove();
			},
			error: function() {
				console.log('error');
			}
		}
		);
	},
	render: function(){
		return (<section>
			<button onClick={this.handleClick}>x</button>
			<h1>Welcome!</h1>
			<p>Thank you for visiting my website. There are still some rough edges I am working through, and if you are here this early in the process, then I want to personally welcome you!</p>
			<p>What you see here is mostly intended as a tech showcase for those interested in that part of my work.</p> <p>If you have any questions, please feel free to call me at +49 170 838 4945 or e-mail me at the contact page.</p>
			</section>)
	}
});

console.dir($('#alertmodal'));
if ($('#alertmodal').length){
	React.render(<NotReady />, document.getElementById('alertmodal'));
}
