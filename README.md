# The readme.


## Locales
There are three "Locals" that are kept track of:

* Phycale
* Locale
* Reqcale

Phycale
	: The physical location that an IP is calling from (not yet implimented)
Locale
	: The machine's prefered language, or - the native language of the user
Reqcale
	: The actually requested page, page dependent localizatons are set with this.

> Supported Locales and translated strings in the page.yml file must always be in parity.


## Translations
String Values returned from the database are translated in database (as of yet a yaml file).
Strings used in a template will be translated in a generic strings table.




## Risks?
Check include function with importing markdown and then rendering them in the swig files (like in history @home) - make sure no script injection risks exist.