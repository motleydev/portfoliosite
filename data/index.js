var yaml 	=	require('js-yaml'),
fs		=	require('fs'),
_ 		=	require('lodash'),
marked	=	require('marked');
//sizer	=	require('image-size');



/**
*
* Structure/Logic related databases
*
**/

try {
	var pages = yaml.safeLoad(fs.readFileSync(__dirname+'/pages.yml', 'utf8'));
} catch (e) {
	console.log(e);
}

try {
	var template = yaml.safeLoad(fs.readFileSync(__dirname+'/template.yml', 'utf8'));
} catch (e) {
	console.log(e);
}

module.exports.template = template;
module.exports.pages = pages;

/**
*
* Content related databases
*
**/


try {
	var brands = yaml.safeLoad(fs.readFileSync(__dirname+'/brands.yml', 'utf8'));
} catch (e) {
	console.log(e);
}


try {
	var otherSkills = yaml.safeLoad(fs.readFileSync(__dirname+'/other-skills.yml', 'utf8'));
} catch (e) {
	console.log(e);
}

try {
	var quotes = yaml.safeLoad(fs.readFileSync(__dirname+'/quotes.yml', 'utf8'));
} catch (e) {
	console.log(e);
}

try {
	var leadText = yaml.safeLoad(fs.readFileSync(__dirname+'/lead-text.yml', 'utf8'));
} catch (e) {
	console.log(e);
}




/**
*
* Transform brand data, parse markdown, csvs, etc
*
**/


function pTheMd(file){

	marked.setOptions({
		renderer: new marked.Renderer(),
		gfm: true,
		tables: true,
		breaks: false,
		pedantic: false,
		sanitize: false,
		smartLists: true,
		smartypants: false
	});

	var doc = fs.readFileSync(__dirname+'/content/'+file, 'utf8');

	return marked(doc);
};


module.exports.brands = brands.map(function(brand){


	_.map(brand.brand_description, function(descript, key){
		brand.brand_description[key] = pTheMd(brand.id+'_'+descript);
	});
	_.map(brand.brand_audience, function(descript, key){
		brand.brand_audience[key] = pTheMd(brand.id+'_'+descript);
	});

	_.map(brand.colateral, function(image, key){
		function Size(object){
			this.height = object.height;
			this.width = object.width;
			this.ratio = (this.height/this.width);
		};
		//image.size = new Size(sizer(_dirname'/images/'+image.image))
	});

	return brand;

});


module.exports.slides = fs.readdirSync(__dirname+'/content/slides').map(function(file){

	function SlideConstructor(markdown, frontmatter){

		var self = this;

		self.content = markdown;
		self.meta = frontmatter;

	};

	var fileContents = fs.readFileSync(__dirname+'/content/slides/'+file, 'utf8');

	try {
		var frontMatter = yaml.safeLoad(fileContents.split('---')[1]);
	} catch (e) {
		console.log(e);
	}

	var bodyContent = marked(fileContents.split('---')[2]);

	return new SlideConstructor(bodyContent, frontMatter);

});

module.exports.leadText = _.map(leadText, function(chunk, index){

	/**
	TODO:
	- Recfactor this code
	**/

	function LeadText(page, content){
		var self = this;
		this.name = page;
		this.content = {};
		_.map(content, function(item, key){
			self.content[key] = {};
			_.map(item, function(newitem, newkey){
				var arrayValues = newitem.split('\n');
				self.content[key][newkey] = _.pull(arrayValues, '');
			});
		});
	}

	return new LeadText(index, chunk);
});


module.exports.otherSkillCategories = otherSkills.categories;
module.exports.otherSkills = otherSkills.other_skills.map(function(skill){
	skill.category = otherSkills.categories['cat_'+skill.category];
	skill.level = otherSkills.skill_levels['cat_'+skill.skil_level];
	skill.category[skill.category.id] = true;
	skill.content = _.transform(skill.content, function(result, lang, key){
		result[key] = marked(lang);
	});
return skill;
});



module.exports.quoteAuthors = quotes.authors;
module.exports.quotes = _.shuffle(quotes.quotes.map(function(quote, index){
	quote.source = quotes.authors[quote.source];
	if(!quote.source){
		console.log("!!!!   "+quote[0])
	}
	else {
		quote.id = quote.source.id +'_'+ index;	
	}
	
	return quote;
}));
