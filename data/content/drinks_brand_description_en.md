Hofgut Storzeln GmbH stands for a few key values: organic, vegan and regional. Beyond conveying splashing pieces of fruit falling into a bowl of milk - we needed to convey these wholesome values. The logo was reworked to incorporate more buildings (a value unique to a "hof") and the raw product was put on front and center display.

* Sans serif packaging modernizes the design but traditional serifs are used in the advertising.
* White is used extensively to re-iterate the "light" and "healthy" core value.
* Bright colors are used to provide a flavor distinction and imply "variation" in the natural product.