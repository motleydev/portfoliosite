The design makes heavy use of serif fonts and ceremonial details like sprawling banners and detailed repeating patterns found in vintage high end fashion. The idea is to convey both a wholesome, traditional image while maintaining a "king of organic snacks" image. Without competitor in quality and taste the design needed to speak for itself and not attempt to "compete" with movie theater popcorn.

* Serif font reinforces traditional branding - also reflected in the historical brand name.
* Fine detailed pattern of a repeating logo in the background provides a flourish of "premium."