The design allows for personal interpretation ranging from a chic social drink / party mixer while still  looking comfortable on the desk of a middle-manager in a busy office. Borrowing from the sans-serif "café" style logo marks and combining with the thin treatment on the word "soy", the initial indication is that this is a classic coffee drink with a fresh and lighter take than previous renditions.  
The colors are intended to pay homage to espresso culture wit fresh, organic highlights.

* Sans serif reflects cafe culture
* Script font adds "crafted" component
* Thin letter treatments imply "light"