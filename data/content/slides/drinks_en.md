---
locale: en
hero_image: drinks-package.jpg
id: drinks
---

## Hofgut Storzeln Drinks
Whether you are a vegan hipster in Berlin or retired financial advisor at the Bodensee - everyone seems to understand the wholesome approach to our branding. When you start with a product this honest, the best marketing is transparency. As our first product in the market, it was critical to represent both the product's inherent value and our company's commitment to organic, sustainable agriculture - a feat that has served as the door opener for all our subsequent brands and products.