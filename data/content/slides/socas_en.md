---
locale: en
hero_image: socas-package.jpg
id: socas
---

## Socas Protein Flakes
The every man product - a branding challenge if ever there was one. How do you target retirement communities, endurance athletes, busy mom and dads and upbeat contemporary chefs with the same brand? Apparently like this - while the initial education efforts have been careful, we are now seeing the said communities realize the power of this little product. It can work in soup and salads, protein bars and protein shakes, morning cereal and late night snack! The challenge has been to not limit the possibilities.