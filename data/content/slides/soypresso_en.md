---
locale: en
hero_image: soypresso-package.jpg
id: soypresso
---

## Soypresso Medium
It's a coffee drink for the rest of us. Who would have thought that there were so few non-dairy "ready-to-go" coffee drink options available? Add to that organic? We developed a real crowd pleaser with Soypresso. Paying homage to a vast histoy of coffee branding before us and putting a slight modern edge for the newer consumers, we have created a product that can sit on the desk of a fortune 500 employee or stick out of community college kid's backpack. If you like coffee drinks and try to drink lactose-free or vegan, you'll love Soypresso.3