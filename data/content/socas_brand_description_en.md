A product that is part wight-control, part sport nutrition, part cooking ingredient and part edible cereal flake in its own right is an interesting brand challenge!    
The "protein flakes" product name is intended to be the first in a series where the accent color will vary with the alternative flavors. To initially educate the customer on the healthful nature of the core product we chose a bright green to lend both health and excitement/innovation to the image. A strong, grid based style juxtaposed with plenty of white space keeps the brand both technical but friendly.

* Providing plenty of scientific information allows the customer to both self educate and feel that they are making the intelligent choice themselves.
* Bright green conveys both health and energy.
* Geometric sans keeps seriousness of content but reflects lighter nature.
