var dotenv = require('dotenv');
dotenv.load();

var sendgrid_username   = process.env.SENDGRID_USERNAME;
var sendgrid_password   = process.env.SENDGRID_PASSWORD;
var to                  = process.env.TO;

var sendgrid   = require('sendgrid')(sendgrid_username, sendgrid_password);



var mailer = function(name, email, web, message){

	var email      = new sendgrid.Email();
	var subject = "Contact from: " + name;
	var body = "Name: "+ name + ", Website: "+ web +", Message: "+ message;
	email.addTo(to);
	email.setFrom(to);
	email.setSubject(subject);
	email.setText(body);
	//email.setHtml('<strong>%how% are you doing?</strong>');
	//email.addSubstitution("%how%", "Owl");
	email.addHeader('X-Sent-Using', 'SendGrid-API');
	email.addHeader('X-Transport', 'web');
	//email.addFile({path: './gif.gif', filename: 'owl.gif'});

	sendgrid.send(email, function(err, json) {
		if (err) { return console.error(err); }
		console.log(json);
	});

}

module.exports = mailer;