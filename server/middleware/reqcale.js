var express = require('express'),
    app     = express();
var config = require('../settings').config;

var session =   require('express-session');

var locale_routes  = express.Router();



config.supportedLocales.forEach(function(route) {
	
	locale_routes.use('/'+route, function(req,res,next){
  		req.session.req_locale = ''+route+'';
  		next();
	});

});


module.exports = locale_routes;
