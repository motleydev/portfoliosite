var _       = require('lodash');
var express = require('express'),
    app     = express(),
    bodyParser = require('body-parser');

var session =   require('express-session');

var auth  = express.Router();

var urlencodedParser = bodyParser.urlencoded({ extended: false })

var jsonParser = bodyParser.json()



auth.post('/login', urlencodedParser, function(req,res,next){
	if(req.body.password === '1234'){
		req.session.auth = true;
		res.redirect(req.session.requestedPage);
	}
	else
	{
		console.log("nope");
	}
});

auth.get('/login', function(req,res){
	res.render("login.html");
});

auth.use('/', function(req,res,next){
  	if(!req.session.auth){
  		req.session.requestedPage = req.url;
  		res.redirect('/login')
  	}
  	else
  	{
  		next();	
  	}
  	
});


module.exports = auth;