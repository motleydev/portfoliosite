var _       = require('lodash');
var express = require('express'),
    app     = express();

var session =   require('express-session');

var inferred_locale  = express.Router();
	
inferred_locale.use('/', function(req,res,next){
  	req.session.inferred_locale = req.headers['accept-language'];
  	next();
});


module.exports = inferred_locale;
