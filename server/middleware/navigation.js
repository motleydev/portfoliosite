/**
*
* XenoLinks are explicitely modified in horse_stall.js
to append route parameters to the transferlink - if you
modify here, please modify there.
*
**/



var _       = require('lodash');
var express = require('express'),
app     = express();

var navigation  = express.Router();

var pages = require('../../data').pages;


navigation.use('/:lang/:page*',function(req,res,next){
	
	var locale = req.session.req_locale;
	
	res.locals.xenoLinks = {};
	res.locals.locale = locale;

	res.locals.mainMenu = [];
	res.locals.secondMenu = [];

	_.forEach(pages, function(nav){

		nav[locale].active = req.params.page === nav[locale].urlSegment;

		// Push other locales for transfer

		if (nav[locale].active === true){
			var xenoNav = _.clone(nav);
			delete xenoNav.location
			res.locals.xenoLinks = _.omit(xenoNav, locale);
		}
		
		if(!nav.offline && nav.location !== 'menu2' && nav.location !== 'hidden'){res.locals.mainMenu.push(nav[locale])}

			if(nav.location === "menu2"){res.locals.secondMenu.push(nav[locale])}
				
		});
	
	next();
});

module.exports = navigation;