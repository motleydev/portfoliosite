var config = require('../settings').config;


module.exports = function pageHelper(value){

		var locales = config.supportedLocales;
		var localeLength = locales.length;

		for (var i = 0; i < localeLength; i++) {
	
			this[locales[i]+'Link'] = '/' + locales[i] + '/' + value.urlSegment;
		};
	
};