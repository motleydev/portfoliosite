var _       = require('lodash');
var express = require('express'),
    app     = express();

var session =   require('express-session');

var physical_locale  = express.Router();
	
physical_locale.use('/', function(req,res,next){
  	req.session.physical_locale = req.ip;
  	next();
});


module.exports = physical_locale;