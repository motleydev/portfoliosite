// Locale Stuff
var locale_routes =  require('./reqcale.js'),
	inferred_locale =  require('./locale.js'),
	physical_locale =  require('./phycale.js');

module.exports.locale_routes = locale_routes;
module.exports.inferred_locale = inferred_locale;
module.exports.physical_locale = physical_locale;


// Navigation Stuff
var navigation = require('./navigation.js'),
	pageHelper = require('./pageHelper.js');

module.exports.navigation = navigation;
module.exports.pageHelper = pageHelper;



var auth = require('./authentication.js');

module.exports.auth = auth;