var fs =  require('fs');


module.exports.languages =
fs.readdirSync(__dirname+'/languages').map(function(language){
	return require('./languages/'+language);
});

module.exports.pages =
fs.readdirSync(__dirname+'/pages').map(function(page){
	return require('./pages/'+page);
});
