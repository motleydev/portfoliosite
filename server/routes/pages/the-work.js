/**
*
* Products Page
*
**/


var express = require('express'),
_       = require('lodash'),
path	= require('path'),
app     = express();

var pageTitle =
module
.filename
.slice(__filename
	.lastIndexOf(path.sep)+1, module
	.filename
	.length -3);

var page  = express.Router();
var defaultView = 'home/index.html';
var pageSpecificView = pageTitle+'/'+pageTitle+'.html'


var data = require('../../../data');

var pages = data.pages;
var pageRoutes = pages[pageTitle];


var brands = data.brands;
var slides = data.slides;

var leadText = _.find(data.leadText, { 'name': pageTitle}).content;

var skills = data.otherSkills;
var skillCategories = data.otherSkillCategories;

_.forIn(pageRoutes, function(value, key) {

	var baseUrl = '/' + key + '/' + value.urlSegment;

	page.use(baseUrl, function(req, res, next) {
		res.locals.currentPage = pages[pageTitle][req.session.req_locale];
		res.locals.leadText = leadText[req.session.req_locale];
		next();
	});


	value.subRoutes.forEach(function(subPage){

		if(subPage.urlSegment === 'branding'){

		/**
		*
		* Content Api for Slides
		*
		**/

		

		page.get(baseUrl+'/branding/:brand/slide', function(req,res,next){
			res.locals.slide = _.find(slides, function(slide){
				if(slide.meta.id == req.params.brand && slide.meta.locale == res.locals.locale ){
					return slide;
				}
			});
			res.render('partials/_slide-template.html');
		});

		/**
		*
		* Individual brand page
		*
		**/
		

		page.get(baseUrl+'/branding/:brand', function(req,res,next){
			
			res.locals.slide = _.find(slides, function(slide){
				if(slide.meta.id == req.params.brand && slide.meta.locale == res.locals.locale ){
					return slide;
				}
				
			});

			res.locals.brands = brands;
			res.locals.brand = _.find(brands, { 'id': req.params.brand });
			
			res.render(pageTitle+'/_brand-detail.html');
		});

	}


	page.get(baseUrl+'/'+subPage.urlSegment, function(req,res,next){


		// Push other locales for transfer

		res.locals.brands = brands;
		res.locals.skills = skills;
		res.locals.skillCategories = skillCategories;
		
		res.render(pageTitle+'/'+subPage.routePage);
	});

});

page.get(baseUrl, function(req, res) {
	res.locals.skills = skills;
	res.locals.skillCategories = skillCategories;
	res.render((value.view || pageSpecificView || defaultView));
});

});


module.exports = page;
