/**
*
* Contact
*
**/


var express = require('express'),
_       = require('lodash'),
path	= require('path'),
app     = express();

var mailer = require('../../modules/mailer');

var bodyParser = require('body-parser');


var pageTitle =
module
.filename
.slice(__filename
	.lastIndexOf(path.sep)+1, module
	.filename
	.length -3);

var page  = express.Router();


var jsonParser = bodyParser.json();

//app.use( bodyParser.json() ); 


var defaultView = 'home/index.html';

var data = require('../../../data');

var pageSpecificView = pageTitle+'/'+pageTitle+'.html'
var leadText = _.find(data.leadText, { 'name': pageTitle}).content;

var pages = data.pages;
var pageRoutes = pages[pageTitle];

_.forIn(pageRoutes, function(value, key) {

	var baseUrl = '/' + key + '/' + value.urlSegment;

	page.get(baseUrl, function(req, res) {
		res.locals.leadText = leadText[req.session.req_locale];
		res.locals.currentPage = pages[pageTitle][req.session.req_locale];
		res.render((value.view || pageSpecificView || defaultView));
	});

});

page.post('/contact', jsonParser, function(req, res){
	
	var response = {
		status  : 200,
		success : 'Updated Successfully'
	};
	
	var formSub = req.body;
	//console.log(formSub);
	mailer(formSub.Name, formSub.Email, formSub.Website, formSub.Message);
	
	if (!req.body){
		return res.sendStatus(400)	
	} 
	else
	{
		return res.end(JSON.stringify(response));
	}

});

module.exports = page;
