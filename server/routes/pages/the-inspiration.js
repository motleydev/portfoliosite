/**
*
* Products Page
*
**/


var express = require('express'),
_       = require('lodash'),
path	= require('path'),
app     = express();

var pageTitle =
module
.filename
.slice(__filename
	.lastIndexOf(path.sep)+1, module
	.filename
	.length -3);

var page  = express.Router();
var defaultView = 'home/index.html';
var pageSpecificView = pageTitle+'/'+pageTitle+'.html'


var data = require('../../../data');

var pages = data.pages;
var pageRoutes = pages[pageTitle];


var quotes = data.quotes;
var authors = data.quoteAuthors;

_.forIn(pageRoutes, function(value, key) {

	var baseUrl = '/' + key + '/' + value.urlSegment;

	page.get(baseUrl, function(req, res) {
		res.locals.quotes = quotes;
		res.locals.authors = authors;
		res.locals.currentPage = pages[pageTitle][req.session.req_locale];
		res.render((value.view || pageSpecificView || defaultView));
	});

});


module.exports = page;
