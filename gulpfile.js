var	gulp 	=	require('gulp'),
server	=	require('browser-sync'),

stylus	=	require('gulp-stylus'),

brwsrfy	=	require('browserify'),
wrap 	= 	require('gulp-wrap'),
reactify = 	require('reactify'),

fs 		= 	require('fs'),
plumber	=	require('gulp-plumber'),
util	=	require('gulp-util'),
cache 	=	require('gulp-cached'),
del 	= 	require('del'),
notify 	=	require('node-notifier'),
gFilter = 	require('gulp-filter'),
flatten = 	require('gulp-flatten');

var gIf 	= 	require('gulp-if');
var source = require('vinyl-source-stream');

var src = {
	raw: {
		images:		'app/images',
		scripts: 	'app/scripts',
		styles: 	'app/styles',
		views: 		'app/views',
		root: 		'app'
	},
	compiled: {
		images:		'build/public/images',
		scripts: 	'build/public/scripts',
		styles: 	'build/public/styles',
		views: 		'build/views',
		root: 		'build'
	}
};


/**
*
* Html tasks
*
**/


var htmlTasks = [];
var htmlFiles = ['pages'];

var condition = function(file){
	return true;
}

htmlFiles.forEach(function(target){
	var taskName = 'html-'+target;
	htmlTasks.push(taskName);
	
	gulp.task(taskName,function(){
		var filter = gFilter(['**',"!**/_**"]);
		gulp.src(src.raw.views+'/'+target+'/**/*.html')
		.pipe(gIf(condition, gulp.dest(src.compiled.views)))
		.pipe(filter)
		.pipe(filter.restore())
		.pipe(gulp.dest(src.compiled.views));
	})
});

gulp.task('html',htmlTasks,function(){
	gulp.src([
		src.raw.views+'/layouts/**.*',
		src.raw.views+'/partials/**.*'
		], {base: 'app/views'})
	.pipe(gulp.dest(src.compiled.views));
});

/**
*
* Styles Task
*
**/

var jeet 	=	require('jeet'),
rupture	=	require('rupture'),
axis	=	require('axis'),
typeset	=	require('typeset'),
type	=	require('typographic'),
katou	=	require('kouto-swiss');

gulp.task('styles', function() {
	gulp.src([src.raw.styles+'/*.styl'])
	.pipe(stylus({
		use: [
		axis(),
		jeet(),
		rupture(),
		type(),
		typeset(),
		katou()
		]
	}))
	.pipe(gulp.dest(src.compiled.styles));
});



/**
*
* Images Task
*
**/

gulp.task('images', function(){
	gulp.src([src.raw.images+'/**/*.jpg', src.raw.images+'/**/*.png', src.raw.images+'/**/*.svg' ,src.raw.images+'/**/*.gif'])
	.pipe(flatten())
	.pipe(gulp.dest(src.compiled.images))
});


gulp.task('scripts', function(){
	var b = brwsrfy();

	b.transform(reactify);
	b.add(['./'+src.raw.scripts+'/main.js','./'+src.raw.scripts+'/manifesto-page.js']);
	
	return b.bundle()
	.on('error', onError)
	.pipe(source('main.js'))
	.pipe(gulp.dest(src.compiled.scripts));
});



/**
*
* House Cleaning
*
**/


gulp.task('build',
	['images', 'scripts', 'styles', 'html'],
	function(){
		util.log('Initial Build');
	});


/**
*
* Watch - app Folder
*
**/

gulp.task('watch', ['build'] ,function() {

	gulp.watch(src.raw.scripts+'/**/*', ['scripts']);
	gulp.watch(src.raw.images+'/**/*', ['images']);
	gulp.watch(src.raw.styles+'/**/*', ['styles']);
	gulp.watch([
		src.raw.views+'/**/*'
		], ['html']);

	gulp.watch([
		src.compiled.root+'/**/*',
		'server/**',
		'data/**/*.yml',
		'data/**/*.md'
		], ['browser-sync-reload']);
});


/**
*
* Error Handler
*
**/

var standardErrorHandler = function(err)
{
	// Needs brew or gem install terminal-notifier
	util.beep();
	notify.notify({mesage: "Error!: " + err.message });
	util.log(util.colors.red('Error'), err.message);
};

var onError = function(err){
	standardErrorHandler(err)
	this.emit('end');
};



/**
*
* Server
*
**/


gulp.task('browser-sync-reload', function() {
	server.reload();
});


gulp.task('browser-sync', ['watch'], function() {
	server({
		proxy: 'localhost:5100',
		startPath: "/en/the-work",
		files: src.compiled.styles+'/main.css',
		open: true,
		injectChanges: true
	});
});



gulp.task('default', ['browser-sync']);
