var express =   require('express');
var app     =   express();
var session =   require('express-session');
var favicon = require('serve-favicon');
var subdomain = require('express-subdomain');
var MongoStore = require('connect-mongo')(session);

//var stylus	=	require('stylus'),
//kouto 	=	require('kouto-swiss'),
//jeet    = require('jeet');

var ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
var port    =   process.env.OPENSHIFT_NODEJS_PORT || 3000;
var swig    =   require('swig');
var swigExtras    =   require('swig-extras');
var fs      =   require('fs');


['markdown','nl2br','truncate'].forEach(function(plugin){
  swigExtras.useFilter(swig, plugin);
});

['markdown'].forEach(function(plugin){
  swigExtras.useTag(swig, plugin);
});

swig.setDefaults({ loader: swig.loaders.fs(__dirname + '/templates/content' )});
swig.setDefaults({ loader: swig.loaders.fs(__dirname + '/build/views/layouts' )});


// routes
var routes  =   require('./server/routes');

//middlewares
var helpers =  require('./server/middleware');


// Some Locals
app.locals.navigation = require('./data').pages;

//View Layer
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/build/views');

// Enable caching for production
app.set('view cache', false);
swig.setDefaults({ cache: false });

app.use(express.static(__dirname + '/build/public'))
app.use(favicon(__dirname + '/build/public/images/favicon.png'));


var env = process.env.NODE_ENV || 'development';
if ('development' == env) {
 var url = process.env.MONGO_DB_URL || 'mongodb://localhost/test-app';
}
else
{
  var url = 'mongodb://' + process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" + process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" + process.env.OPENSHIFT_MONGODB_DB_HOST + ':' + process.env.OPENSHIFT_MONGODB_DB_PORT + '/' + process.env.OPENSHIFT_APP_NAME;
}

app.use(session({
 secret: 'keyboard cat',
 cookie: {maxAge: 90000000},
 resave: true,
 saveUninitialized: false,
 store: new MongoStore({
  url: url,
  autoRemove: 'true'
})
}));

// Set Locales


app.use('/', function(req,res,next){
  res.locals.ok = req.session.ok;
  next();
});


app.use('/', [
	helpers.locale_routes,
	helpers.inferred_locale,
	helpers.physical_locale]);

app.use('/', function(req,res,next){
  res.header("Content-Type", "text/html; charset=utf-8");
  next();
});

app.use('/', function(req,res,next){
  if(req.session.req_locale !== req.session.inferred_locale){
    res.locals.locale_alert = {
      message: "Hey, you are viewing the German page, did you mean to view the english page?",
      locale: req.session.inferred_locale
    }
    //console.log(res.locals.locale_alert);
  }
  next();
});

// auth layer
//app.use('/', helpers.auth);
app.use('/', routes.languages);
app.use('/', helpers.navigation);
app.use('/', routes.pages);

app.get('/style', function(req,res){
  res.render('styleguide.html')
});

app.get('/', function(req, res){
  res.redirect('/en/the-work');
});


app.post('/dismiss', function(req, res, next){
  //console.log('dismissed');
  req.session.ok = true;
  var response = {
    status  : 200,
    success : 'Updated Successfully'
  };
  res.end(JSON.stringify(response));
});

app.use(function(req, res, next){
  res.status(404);
  res.redirect('/'+(req.session.req_locale || 'en' )+'/404');
});


app.use(function(err, req, res, next) {
  res.status(500);
  //console.log(err.stack)
  res.redirect('/'+(req.session.req_locale || 'en' )+'/500');
});


app.listen(port, ip);
console.log('Magic happens on port ' + port);
